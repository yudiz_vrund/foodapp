import React from "react";
import { NavLink, useParams } from "react-router-dom";
import PropTypes from "prop-types";

const ShowSubCategory = ({ Subcategory }) => {
  const { CatID } = useParams();

  return (
    <>
      <div className="subStyle">
        <NavLink
          to={`/${CatID}/${Subcategory.id}`}
          key={Subcategory.id}
          className="subLine"
          activeClassName="activeSub"
        >
          {Subcategory.name}
        </NavLink>
      </div>
    </>
  );
};

ShowSubCategory.propTypes = {
  Subcategory: PropTypes.shape({
    id: PropTypes.number,
    name: PropTypes.string,
  }).isRequired,
};

export default ShowSubCategory;
