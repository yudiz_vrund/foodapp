import React from "react";
import { FormattedMessage } from "react-intl";
import "../Assets/CSS/Category.css";

const Info = () => {
  return (
    <>
      <div>
        <p className="header1">
          <FormattedMessage
            id="mainTitle"
            defaultMessage="Kings Arms Cardington"
          />
        </p>
        <p className="header2">
          <FormattedMessage
            id="titleAddress1"
            defaultMessage="134 High Street, Kempston,"
          />
          <br />
          <FormattedMessage
            id="titleAddress2"
            defaultMessage="Bedford Bedfordshire, MK42 7BN"
          />
        </p>
      </div>
    </>
  );
};

export default Info;
