import React from "react";
import { NavLink } from "react-router-dom";
import PropTypes from "prop-types";

const ShowCatergory = ({ category }) => {
  return (
    <>
      <div className="flexItems">
        <NavLink
          to={`/${category.id}`}
          key={category.id}
          className="categoryStyle"
          activeClassName="catActive"
        >
          {category.name}
        </NavLink>
      </div>
    </>
  );
};

ShowCatergory.propTypes = {
  category: PropTypes.shape({
    id: PropTypes.number,
    name: PropTypes.string,
  }).isRequired,
};

export default ShowCatergory;
