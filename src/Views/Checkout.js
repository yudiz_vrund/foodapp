import React, { useEffect, useState } from "react";
import Container from "@material-ui/core/Container";
import { Table, Button, Modal, ModalBody } from "reactstrap";
import { useSelector, useDispatch } from "react-redux";
import { FormattedMessage } from "react-intl";
import "../Assets/CSS/Checkout.css";
import { useHistory } from "react-router";
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from "@material-ui/icons/Delete";
import { RemoveFromCart, EmptyCart } from "../Redux/Actions/CartAction";
import CheckoutHeader from "./CheckoutHeader";

const Checkout = () => {
  const cartItems = useSelector((state) => {
    return state.Cart;
  });

  const [price, setPrice] = useState(0);
  const [cartlength, setCartLength] = useState(0);

  const [modal, setModal] = useState(false);
  const toggle = () => {
    return setModal(!modal);
  };

  useEffect(() => {
    setCartLength(cartItems.cart.length);
    setPrice(cartItems.totalAmount.toFixed(2));
  }, [cartItems]);

  const history = useHistory();

  const dispatch = useDispatch();

  const Remove = (id, total) => {
    dispatch(RemoveFromCart(id, total));
  };

  const empty = () => {
    history.replace("/");
    dispatch(EmptyCart());
  };

  return (
    <>
      <Container maxWidth="sm">
        <CheckoutHeader />
        <Table borderless>
          {cartItems.cart.map((items) => {
            const totalExtra = items.checkeditem
              .map((checkboxItem) => {
                return checkboxItem.price;
              })
              .reduce((a, b) => {
                return a + b;
              }, 0);

            let total = 0;
            if (items.radio.price > 0) {
              total = (items.radio.price + totalExtra) * items.quantity;
            } else {
              total = items.radio * items.quantity;
            }

            return (
              <>
                <tr className="itemTitle">
                  <td>
                    {items.quantity} x {items.name}
                  </td>
                  <td> £ {total}</td>
                  <td>
                    <IconButton aria-label="delete">
                      <DeleteIcon
                        fontSize="medium"
                        onClick={() => {
                          Remove(items.primaryKey, total);
                        }}
                      />
                    </IconButton>
                  </td>
                </tr>
                <tr>
                  <td colSpan="2" className="variantStyle">
                    {items.radio.name}{" "}
                    {items.checkeditem.map((checkitem) => {
                      return `, ${checkitem.name}`;
                    })}
                  </td>
                </tr>
              </>
            );
          })}
        </Table>

        <hr className="linewidth" />
        <div className="addNote">
          {/* eslint-disable-next-line jsx-a11y/label-has-associated-control */}
          <label htmlFor="add">Add note : </label>
          <textarea name="add" id="add" className="noteArea" />
        </div>
        <hr className="linewidth" />
        <div className="tablenumberDiv">
          <p tag="h1">
            <FormattedMessage id="tableTitle" defaultMessage="Table Number" />
            <Button
              className="tblButton"
              size="md"
              block
              color="warning"
              outline
            >
              32
            </Button>
          </p>
        </div>
        <br />
        <Button
          className="myCheckButton"
          size="lg"
          block
          color="warning"
          onClick={() => {
            setModal(true);
          }}
        >
          <span>
            <FormattedMessage
              id="confirmOrder"
              defaultMessage="Confirm Order"
            />
          </span>
          <br />
          <span>
            £ {price} / {cartlength}{" "}
            <FormattedMessage id="basketItem" defaultMessage="ITEM" />
          </span>
        </Button>

        <Modal isOpen={modal} toggle={toggle}>
          <ModalBody>
            <h2 className="successText">
              <FormattedMessage
                id="successMsg"
                defaultMessage="Order Successful"
              />
              !
            </h2>
            <img
              className="myImg"
              src="https://media.istockphoto.com/vectors/tick-icon-vector-symbol-vector-id971623860?k=6&m=971623860&s=612x612&w=0&h=WIkXzhiOjBk6YqWGIbzo-_ttLKbDnK0y2GsYGfGEVAA="
              alt=""
            />
            <h6 className="cText">
              <FormattedMessage
                id="successPara"
                defaultMessage="By placing this order you agree that you are present in Kings Arms and over 18 years old."
              />{" "}
            </h6>
            <Button
              className="cButton"
              color="warning"
              onClick={() => {
                empty();
              }}
            >
              <FormattedMessage id="homeBtn" defaultMessage="Back to Home" />
            </Button>
          </ModalBody>
        </Modal>
      </Container>
    </>
  );
};

export default Checkout;
