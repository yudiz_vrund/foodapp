import React from "react";
import { FormattedMessage } from "react-intl";

const CheckoutHeader = () => {
  return (
    <>
      <div>
        <h2 className="checkoutTitle">
          <FormattedMessage id="checkout" defaultMessage="Checkout" />
        </h2>

        <h5 className="check1">
          <FormattedMessage
            id="checkoutTitle"
            defaultMessage="Kempston Hammers Sports & Social Club"
          />
        </h5>
        <p className="check2">
          <FormattedMessage
            id="titleAddress1"
            defaultMessage="134 High Street, Kempston,"
          />
          <br />
          <FormattedMessage
            id="titleAddress2"
            defaultMessage="Bedford Bedfordshire, MK42 7BN"
          />
        </p>
      </div>
    </>
  );
};

export default CheckoutHeader;
