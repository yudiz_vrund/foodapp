/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from "react";
import { useParams, useHistory } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { FormattedMessage } from "react-intl";

import {
  Card,
  CardBody,
  CardTitle,
  CardSubtitle,
  CardText,
  Table,
  Modal,
  ModalBody,
  Button,
  Input,
  Label,
} from "reactstrap";
import "../Assets/CSS/ListData.css";
import { AddToCart } from "../Redux/Actions/CartAction";

const ListData = () => {
  const { SubID } = useParams();

  const product = useSelector((state) => {
    return state.Product;
  });
  const loadedProducts = product.Product.products;

  const categories = useSelector((state) => {
    return state.Category;
  });
  const loadedCategory = categories.Category;

  const [modal, setModal] = useState(false);
  const [pid, setPid] = useState(0);

  const [radio, setRadio] = useState({});
  const [checkeditem, setCheckedItem] = useState([]);
  const [quantity, setQuantity] = useState(1);

  const [canvas, setCanvas] = useState(false);

  const toggle = () => {
    return setModal(!modal);
  };

  const openModel = (id) => {
    setModal(true);
    setPid(id);
  };

  const cartItems = useSelector((state) => {
    return state.Cart;
  });

  const [cartlength, setCartLength] = useState(0);
  const [price, setPrice] = useState(0);
  const [show, setShow] = useState(false);

  const dispatch = useDispatch();

  const [parent, setParent] = useState();

  const Getdata = (name, id, parentID) => {
    if (quantity > 0) {
      const initId = 1;

      const primaryKey = cartlength + initId;

      const load = loadedCategory.filter((cat) => {
        // eslint-disable-next-line eqeqeq
        return parentID == cat.id;
      })[0].parent;

      const loadNew = loadedCategory.filter((item) => {
        // eslint-disable-next-line eqeqeq
        return load == item.id;
      })[0].name;

      const CartData = {
        primaryKey,
        id,
        parentID,
        loadNew,
        name,
        radio,
        checkeditem,
        quantity,
      };

      const totalExtra = CartData.checkeditem
        .map((checkboxItem) => {
          return checkboxItem.price;
        })
        .reduce((a, b) => {
          return a + b;
        }, 0);

      let TotalAmount = 0;
      if (radio.price > 0) {
        TotalAmount = (CartData.radio.price + totalExtra) * CartData.quantity;
      } else {
        TotalAmount = CartData.radio * CartData.quantity;
      }

      dispatch(AddToCart(CartData, TotalAmount));
      setRadio({});
      setCheckedItem([]);
      setQuantity(1);
      setModal(false);
    } else {
      alert("Please enter the quantity or pint");
    }
  };

  const history = useHistory();

  useEffect(() => {
    setCartLength(cartItems.cart.length);
    setPrice(cartItems.totalAmount);
  }, [cartItems]);

  return (
    <>
      <br />
      <ul>
        {loadedProducts.map((lProduct) => {
          // eslint-disable-next-line eqeqeq
          return lProduct.parentId == SubID ? (
            <div className="Card ">
              <Card
                outline
                color="warning"
                onClick={() => {
                  return openModel(lProduct.id);
                }}
                className="border-0"
              >
                <CardBody className="CardStyle">
                  <Table>
                    <tr>
                      <td>
                        <CardTitle tag="h5" className="darkBig">
                          {lProduct.name}
                        </CardTitle>
                      </td>
                      <td rowSpan="2">
                        <CardText tag="h4" className="rightoncard">
                          £{lProduct.price}
                        </CardText>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <CardSubtitle className="descriptionText">
                          {lProduct.description}
                        </CardSubtitle>
                      </td>
                    </tr>
                  </Table>
                </CardBody>
              </Card>
            </div>
          ) : null;
        })}
      </ul>
      <Modal isOpen={modal} toggle={toggle}>
        {loadedProducts.map((Newproduct) => {
          return pid === Newproduct.id ? (
            <ModalBody>
              <Table>
                <tr>
                  <td colSpan="3">
                    <h3 className="pName">{Newproduct.name}</h3>
                  </td>
                </tr>
                <tr>
                  <td className="hr" />
                </tr>
                <tr>
                  <td colSpan="3">
                    <h4 className="pSize">
                      <FormattedMessage id="size" defaultMessage="Size" />
                    </h4>
                  </td>
                </tr>
                {Newproduct.variants ? (
                  Newproduct.variants.map((variant) => {
                    return variant ? (
                      <tr>
                        <td>
                          <td colSpan="3" className="myRadio">
                            <Label check className="checkLable">
                              <Input
                                className="check"
                                type="radio"
                                name="radio2"
                                onClick={() => {
                                  return setRadio(variant);
                                }}
                              />
                              {variant.name} £ {variant.price}
                            </Label>
                          </td>
                        </td>
                      </tr>
                    ) : null;
                  })
                ) : (
                  <tr>
                    <td>
                      <td colSpan="3" className="myRadio">
                        <Label check className="checkLable">
                          <Input
                            className="check"
                            type="radio"
                            name="radio2"
                            onClick={() => {
                              return setRadio(Newproduct.price);
                            }}
                          />
                          {Newproduct.name} £ {Newproduct.price}
                        </Label>
                      </td>
                    </td>
                  </tr>
                )}
                <tr>
                  <td>
                    <br />
                  </td>
                </tr>
                <tr>
                  <td className="hr" />
                </tr>
                <tr>
                  <td colSpan="3">
                    <h4 className="option">
                      <FormattedMessage
                        id="select"
                        defaultMessage="Select Options"
                      />
                    </h4>
                  </td>
                </tr>
                {Newproduct.extras?.map((extras) => {
                  return extras ? (
                    <tr>
                      <td>
                        <h6 className="extra">
                          {extras.name} (+ £{extras.price})
                        </h6>
                      </td>
                      <td colSpan="1" />
                      <td>
                        <Input
                          type="checkbox"
                          onClick={() => {
                            return setCheckedItem((prev) => {
                              return [...prev, extras];
                            });
                          }}
                        />
                      </td>
                    </tr>
                  ) : null;
                })}
                <tr>
                  <td className="hr" />
                </tr>
                <tr>
                  <br />
                </tr>
              </Table>
              <div className="container_button">
                <Button
                  className="container_button_items common"
                  onClick={() => {
                    return quantity > 0
                      ? setQuantity(quantity - 1)
                      : alert("Negative not allowed");
                  }}
                >
                  {" "}
                  -{" "}
                </Button>
                <Button
                  className="container_button_items diff"
                  outline
                  color="warning"
                >
                  {" "}
                  {quantity}{" "}
                </Button>
                <Button
                  className="container_button_items common"
                  onClick={() => {
                    return setQuantity(quantity + 1);
                  }}
                >
                  {" "}
                  +{" "}
                </Button>
              </div>
              <br />
              <Button
                size="lg"
                className="orderyellowButton"
                onClick={() => {
                  return Getdata(
                    Newproduct.name,
                    Newproduct.id,
                    Newproduct.parentId
                  );
                }}
              >
                {" "}
                <FormattedMessage id="Addorder" defaultMessage="Add Order" />
              </Button>
            </ModalBody>
          ) : null;
        })}
      </Modal>
      {cartlength > 0 ? (
        <Button
          className="basketButton"
          size="lg"
          block
          color="warning"
          onClick={() => {
            history.replace("/checkout");
          }}
        >
          <span>
            <FormattedMessage id="basketTitle" defaultMessage="View Basket" />
          </span>
          <br />
          <span>
            £ {price} / {cartlength}{" "}
            <FormattedMessage id="basketItem" defaultMessage="ITEM" />
          </span>
        </Button>
      ) : null}
    </>
  );
};

export default ListData;
