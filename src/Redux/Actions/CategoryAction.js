import * as actionTypes from "./Types";

export const AddCategoryArray = (CatData) => {
  return {
    type: actionTypes.ADD_CATEGORY_ARRAY,
    payload: {
      Cat: CatData,
    },
  };
};

export default AddCategoryArray;
