import * as actionTypes from "./Types";

export function english() {
  return {
    type: actionTypes.SELECT_ENGLISH,
  };
}

export function french() {
  return {
    type: actionTypes.SELECT_FRENCH,
  };
}
