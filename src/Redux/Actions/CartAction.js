import * as actionTypes from "./Types";

export const AddToCart = (CartData, TotalAmount) => {
  return {
    type: actionTypes.ADD_TO_CART,
    payload: {
      Cart: CartData,
      TotalAmount: TotalAmount,
    },
  };
};

export const RemoveFromCart = (itemID, total) => {
  return {
    type: actionTypes.REMOVE_FROM_CART,
    payload: {
      id: itemID,
      Total: total,
    },
  };
};

export const EmptyCart = () => {
  return {
    type: actionTypes.EMPTY_CART,
  };
};
