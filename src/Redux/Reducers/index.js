import { combineReducers } from "redux";
import { persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";
import Categories from "./Categories";
import Products from "./Products";
import CartReducer from "./CartReducer";
import LanguageReducer from "./LanguageReducer";

const persistConfig = {
  key: "root",
  storage,
  whitelist: ["Cart", "Language"],
};

const rootReducer = combineReducers({
  Category: Categories,
  Product: Products,
  Cart: CartReducer,
  Language: LanguageReducer,
});

export default persistReducer(persistConfig, rootReducer);
