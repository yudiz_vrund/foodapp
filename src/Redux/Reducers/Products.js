import data from "../../Data/products.json";

export const initialState = {
  Product: data,
};

const Products = (state = initialState, action) => {
  switch (action.type) {
    default:
      return state;
  }
};

export default Products;
