import * as actionTypes from "../Actions/Types";

export const initialState = {
  Language: "English",
};

const LanguageReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.SELECT_ENGLISH:
      return {
        Language: "English",
      };
    case actionTypes.SELECT_FRENCH:
      return {
        Language: "French",
      };
    default:
      return state;
  }
};

export default LanguageReducer;
