import * as actionTypes from "../Actions/Types";

export const initialState = {
  cart: [],
  totalAmount: 0,
};

const CartReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.ADD_TO_CART:
      return {
        ...state,
        cart: [...state.cart, action.payload.Cart],
        totalAmount: state.totalAmount + action.payload.TotalAmount,
      };
    case actionTypes.REMOVE_FROM_CART:
      return {
        ...state,
        cart: state.cart.filter((item) => {
          return item.primaryKey !== action.payload.id;
        }),
        totalAmount: state.totalAmount - action.payload.Total,
      };
    case actionTypes.EMPTY_CART:
      return {
        cart: [],
        totalAmount: 0,
      };
    default:
      return state;
  }
};

export default CartReducer;
