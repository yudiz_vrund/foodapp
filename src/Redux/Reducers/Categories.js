import data from "../../Data/Categories.json";
import * as actionTypes from "../Actions/Types";

export const initialState = {
  Category: data.categories,
  CatArray: [],
};

const Categories = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.ADD_CATEGORY_ARRAY:
      return {
        ...state,
        CatArray: [...state.CatArray, action.payload.Cat],
      };
    default:
      return state;
  }
};

export default Categories;
