import { createStore } from "redux";
import { persistStore } from "redux-persist";
import { composeWithDevTools } from "redux-devtools-extension";
import rootReducer from "./Reducers/index";

export const Store = createStore(rootReducer, composeWithDevTools());

export const persistor = persistStore(Store);

export default { Store, persistor };
