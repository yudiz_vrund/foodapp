import React from "react";
import { useParams } from "react-router-dom";
import { useSelector } from "react-redux";
import ListData from "../Views/ListData";
import ShowSubCategory from "../Views/ShowSubCategory";
import "../Assets/CSS/SubCategory.css";

const Subcategory = () => {
  const categories = useSelector((state) => {
    return state.Category;
  });
  const loadedCategory = categories.Category;
  const { CatID } = useParams();
  const { SubID } = useParams();

  return (
    <>
      <div className="subUl">
        {loadedCategory.map((category) => {
          // eslint-disable-next-line eqeqeq
          if (category.parent == CatID) {
            return <ShowSubCategory Subcategory={category} />;
          }
          return <></>;
        })}
      </div>
      {SubID != null ? <ListData /> : null}
    </>
  );
};

export default Subcategory;
