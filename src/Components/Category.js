import React, { useEffect } from "react";
import Container from "@material-ui/core/Container";
import { useParams, useHistory } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { ButtonGroup, Button } from "reactstrap";
import "../Assets/CSS/Category.css";
import Info from "../Views/Info";
import Subcategory from "./Subcategory";
import { AddCategoryArray } from "../Redux/Actions/CategoryAction";
import ShowCatergory from "../Views/ShowCatergory";
import { english, french } from "../Redux/Actions/LanguageAction";

const Category = () => {
  const categories = useSelector((state) => {
    return state.Category;
  });
  const loadedCategory = categories.Category;

  const { CatID } = useParams();

  const history = useHistory();

  useEffect(() => {
    return CatID === 1 ? history.push("/1/7") : null;
  }, [CatID]);

  const dispatch = useDispatch();

  useEffect(() => {
    const MyCat = loadedCategory
      .filter((cat) => {
        return cat.parent === null;
      })
      .map((item) => {
        return item.name;
      });

    dispatch(AddCategoryArray(MyCat));
  }, []);

  return (
    <>
      <Container maxWidth="sm">
        <div className="topButton">
          <ButtonGroup>
            <Button
              type="button"
              color="warning"
              onClick={() => {
                return dispatch(english());
              }}
            >
              En
            </Button>
            <Button
              color="warning"
              type="button"
              onClick={() => {
                return dispatch(french());
              }}
            >
              Fr
            </Button>
          </ButtonGroup>
        </div>

        <Info />
        <br />
        <div className="CatUl">
          {loadedCategory.map((category) => {
            if (category.parent === null) {
              return (
                <>
                  <ShowCatergory category={category} />
                </>
              );
            }
            return <></>;
          })}
        </div>
        {CatID != null ? <Subcategory /> : null}
      </Container>
    </>
  );
};

export default Category;
