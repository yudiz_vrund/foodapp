import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { IntlProvider } from "react-intl";
import { useSelector } from "react-redux";
import Category from "./Components/Category";
import Checkout from "./Views/Checkout";
import English from "./Languages/en.json";
import French from "./Languages/fr.json";

function App() {
  const message = useSelector((state) => {
    return state.Language;
  });

  let local = "";
  let lang;

  if (message.Language === "English") {
    lang = English;
    local = "en";
  }
  if (message.Language === "French") {
    lang = French;
    local = "fr";
  }

  return (
    <>
      <IntlProvider locale={local} messages={lang}>
        <Router>
          <Switch>
            <Route exact path="/checkout" component={Checkout} />
            <Route exact path="/" component={Category} />
            <Route exact path="/:CatID" component={Category} />
            <Route exact path="/:CatID/:SubID" component={Category} />
          </Switch>
        </Router>
      </IntlProvider>
    </>
  );
}

export default App;
